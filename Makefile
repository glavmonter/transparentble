
APP_VERSION?=1
DFU_PATH?=DFU

DEBUG_PATH=Output/Debug/Exe
RELEASE_PATH=Output/Release/Exe


all: mk debug_dfu release_dfu


mk:
	mkdir -p $(DFU_PATH)

debug_dfu:
	nrfutil pkg generate --hw-version 52 --application-version $(APP_VERSION) --application $(DEBUG_PATH)/Transparent_ReaderChina.hex --sd-req 0xA8 --key-file private.key $(DFU_PATH)/Transparent_ReaderChina_hw52_app${APP_VERSION}_debug.zip
	nrfutil pkg generate --hw-version 53 --application-version $(APP_VERSION) --application $(DEBUG_PATH)/Transparent_ReaderChina.hex --sd-req 0xA8 --key-file private.key $(DFU_PATH)/Transparent_ReaderChina_hw53_app${APP_VERSION}_debug.zip

release_dfu:
	nrfutil pkg generate --hw-version 52 --application-version $(APP_VERSION) --application $(RELEASE_PATH)/Transparent_ReaderChina.hex --sd-req 0xA8 --key-file private.key $(DFU_PATH)/Transparent_ReaderChina_hw52_app${APP_VERSION}_release.zip
	nrfutil pkg generate --hw-version 53 --application-version $(APP_VERSION) --application $(RELEASE_PATH)/Transparent_ReaderChina.hex --sd-req 0xA8 --key-file private.key $(DFU_PATH)/Transparent_ReaderChina_hw53_app${APP_VERSION}_release.zip

clean:
	rm -frv $(DFU_PATH)/*.zip
