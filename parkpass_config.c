

#include <nrf_error.h>
#include <sdk_errors.h>
#include "fds.h"
#include "parkpass_config.h"
#include "rssi_calculator.h"

#define NRF_LOG_MODULE_NAME ppc
#define NRF_LOG_LEVEL       4   // Debug
#define NRF_LOG_INFO_COLOR  2   // Red
#define NRF_LOG_DEBUG_COLOR 2   // Red

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
NRF_LOG_MODULE_REGISTER();


#define CONFIG_FILE     (0xF012)
#define CONFIG_REC_KEY  (0x7010)


/* Array to map FDS return values to strings. */
char const * fds_err_str[] =
{
    "FDS_SUCCESS",
    "FDS_ERR_OPERATION_TIMEOUT",
    "FDS_ERR_NOT_INITIALIZED",
    "FDS_ERR_UNALIGNED_ADDR",
    "FDS_ERR_INVALID_ARG",
    "FDS_ERR_NULL_ARG",
    "FDS_ERR_NO_OPEN_RECORDS",
    "FDS_ERR_NO_SPACE_IN_FLASH",
    "FDS_ERR_NO_SPACE_IN_QUEUES",
    "FDS_ERR_RECORD_TOO_LARGE",
    "FDS_ERR_NOT_FOUND",
    "FDS_ERR_NO_PAGES",
    "FDS_ERR_USER_LIMIT_REACHED",
    "FDS_ERR_CRC_CHECK_FAILED",
    "FDS_ERR_BUSY",
    "FDS_ERR_INTERNAL",
};

/* Array to map FDS events to strings. */
static char const * fds_evt_str[] =
{
    "FDS_EVT_INIT",
    "FDS_EVT_WRITE",
    "FDS_EVT_UPDATE",
    "FDS_EVT_DEL_RECORD",
    "FDS_EVT_DEL_FILE",
    "FDS_EVT_GC",
};


typedef struct
{
    uint32_t   boot_count;
    uint32_t   bleto;
    int8_t     rssi_threshold;
    int8_t     rssi_delta;
    int32_t    parking_id;
    int8_t     name[DEVICE_SIMPLE_NAME_LEN+1];
} configuration_t;


static configuration_t m_dummy_cfg = 
{
    .bleto  = 1000,
    .rssi_threshold  = RSSI_CALC_THRESHOLD,
    .rssi_delta = RSSI_CALC_DELTA,
    .boot_count  = 0x0,
    .parking_id = 0,
    .name = {0x00}
};

static fds_record_t const m_dummy_record = 
{
    .file_id = CONFIG_FILE,
    .key = CONFIG_REC_KEY,
    .data.p_data = &m_dummy_cfg,
    .data.length_words = (sizeof(m_dummy_cfg) + 3) / sizeof(uint32_t),
};


static bool __IO m_fds_initialized = false;
static void fds_evt_handler(fds_evt_t const * p_evt)
{
    NRF_LOG_DEBUG("Event: %s received (%s)",
                  fds_evt_str[p_evt->id],
                  fds_err_str[p_evt->result]);

    switch (p_evt->id)
    {
        case FDS_EVT_INIT:
            if (p_evt->result == FDS_SUCCESS)
            {
                m_fds_initialized = true;
            }
            break;

        case FDS_EVT_WRITE:
        {
            if (p_evt->result == FDS_SUCCESS)
            {
                NRF_LOG_INFO("Record ID:\t0x%04x",  p_evt->write.record_id);
                NRF_LOG_INFO("File ID:\t0x%04x",    p_evt->write.file_id);
                NRF_LOG_INFO("Record key:\t0x%04x", p_evt->write.record_key);
            }
        } break;

        case FDS_EVT_DEL_RECORD:
        {
            if (p_evt->result == FDS_SUCCESS)
            {
                NRF_LOG_INFO("Record ID:\t0x%04x",  p_evt->del.record_id);
                NRF_LOG_INFO("File ID:\t0x%04x",    p_evt->del.file_id);
                NRF_LOG_INFO("Record key:\t0x%04x", p_evt->del.record_key);
            }
        } break;

        default:
            break;
    }
}


void ppc_init() {
ret_code_t err_code;

    fds_register(fds_evt_handler);
    err_code = fds_init();
    APP_ERROR_CHECK(err_code);
    while (!m_fds_initialized)
        sd_app_evt_wait();
    NRF_LOG_INFO("FDS initialized");

    fds_stat_t stat = {0};
    err_code = fds_stat(&stat);
    APP_ERROR_CHECK(err_code);

    fds_record_desc_t desc = {0};
    fds_find_token_t tok = {0};

    err_code = fds_record_find(CONFIG_FILE, CONFIG_REC_KEY, &desc, &tok);
    if (err_code == FDS_SUCCESS) {
        fds_flash_record_t config = {0};
        err_code = fds_record_open(&desc, &config);
        APP_ERROR_CHECK(err_code);

        memcpy(&m_dummy_cfg, config.p_data, sizeof(configuration_t));
        NRF_LOG_INFO("Config file found");
        
        if (NRF_POWER->RESETREAS & (1 << 2)) {
            NRF_LOG_INFO("Soft Reset");
        }
        
        err_code = fds_record_close(&desc);
        APP_ERROR_CHECK(err_code);


        
        NRF_LOG_INFO("BLE Timeout: %d", ppc_get_bleto());
        NRF_LOG_INFO("RSSI Threshold: %d", ppc_get_rssi_threshold());
        NRF_LOG_INFO("RSSI Delta: %d", ppc_get_rssi_delta());
        NRF_LOG_INFO("Parking ID: %d", ppc_get_parking_id());
        NRF_LOG_INFO("Name: %s", ppc_get_name());
    } else {

        NRF_LOG_INFO("Config file not found, writting...");
        strcpy(m_dummy_cfg.name, "Default");
        err_code = fds_record_write(&desc, &m_dummy_record);
        APP_ERROR_CHECK(err_code);
    }

    NRF_POWER->RESETREAS = 0xFFFFFFFF;
}


static ret_code_t update_config() {
ret_code_t err_code;

fds_record_desc_t desc = {0};
fds_find_token_t tok = {0};

    err_code = fds_record_find(CONFIG_FILE, CONFIG_REC_KEY, &desc, &tok);
    if (err_code == FDS_SUCCESS) {
        fds_flash_record_t config = {0};
        err_code = fds_record_update(&desc, &m_dummy_record);
        APP_ERROR_CHECK(err_code);
        return err_code;
    }

    return err_code;
}


uint32_t ppc_get_bleto() {
    return m_dummy_cfg.bleto;
}

ret_code_t ppc_set_bleto(uint32_t bleto) {
    m_dummy_cfg.bleto = bleto;   
    return update_config();
}


int8_t ppc_get_rssi_threshold() {
    return m_dummy_cfg.rssi_threshold;
}

ret_code_t ppc_set_rssi_threshold(int8_t threshold) {
    m_dummy_cfg.rssi_threshold = threshold;
    return update_config();
}


int8_t ppc_get_rssi_delta() {
    return m_dummy_cfg.rssi_delta;
}

ret_code_t ppc_set_rssi_delta(int8_t delta) {
    m_dummy_cfg.rssi_delta = delta;
    return update_config();
}

int32_t ppc_get_parking_id() {
    return m_dummy_cfg.parking_id;
}

ret_code_t ppc_set_parking_id(int32_t parking_id) {
    m_dummy_cfg.parking_id = parking_id;
    return update_config();
}

int8_t *ppc_get_name() {
    return m_dummy_cfg.name;
}

ret_code_t ppc_set_name(const uint8_t *name) {
    strncpy(m_dummy_cfg.name, name, DEVICE_SIMPLE_NAME_LEN);
    return update_config();
}
