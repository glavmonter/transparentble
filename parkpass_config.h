#ifndef _PARKPASS_CONFIG_H_
#define _PARKPASS_CONFIG_H_


#include <nrf_error.h>
#include <sdk_errors.h>
#include "ble_nus.h"

#include "app_uart.h"
#include "app_util_platform.h"

#define PP_CONN_TRIES_MAX               25
#define PP_CLOSSED_DEVICE_TIMEOUT       2000

#define DEVICE_SIMPLE_NAME_LEN          27

void ppc_init();
int8_t ppc_get_rssi_delta();
ret_code_t ppc_set_rssi_delta(int8_t delta);

int8_t ppc_get_rssi_threshold();
ret_code_t ppc_set_rssi_threshold(int8_t threshold);

uint32_t ppc_get_bleto();
ret_code_t ppc_set_bleto(uint32_t bleto);

int32_t ppc_get_parking_id();
ret_code_t ppc_set_parking_id(int32_t parking_id);

int8_t *ppc_get_name();
ret_code_t ppc_set_name(const uint8_t *name);


#endif 
