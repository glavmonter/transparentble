
#define NRF_LOG_MODULE_NAME crypto
#define NRF_LOG_LEVEL       4   // Debug
#define NRF_LOG_INFO_COLOR  2   // Red
#define NRF_LOG_DEBUG_COLOR 2   // Red

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
NRF_LOG_MODULE_REGISTER();

#include "mbedtls/base64.h"
#include "parkpass_crypto.h"

#define USE_DEBUG_KEYS 1

#define CRYPTO_ERROR_CHECK(error)     \
do                                  \
{                                   \
    if (error != NRF_SUCCESS)       \
    {                               \
        NRF_LOG_ERROR("Error 0x%04X: %s", error, nrf_crypto_error_string_get(error));\
    }                               \
} while(0)


static nrf_crypto_ecc_private_key_t m_local_key_private;
static nrf_crypto_ecc_public_key_t m_local_key_public;
static nrf_crypto_ecc_public_key_t m_external_key_public;
static nrf_crypto_ecdh_secp256r1_shared_secret_t m_shared_key;

static uint8_t m_local_key_public_raw[ECDH_PUBLIC_KEY_SIZE];
static uint8_t m_local_key_private_raw[ECDH_PRIVATE_KEY_SIZE];
static uint8_t m_external_key_public_raw[ECDH_PUBLIC_KEY_SIZE];


#if USE_DEBUG_KEYS == 1
const char pubkey_base64[] = "rOMADeFIPtmC2IQyk5fHFnr6jAqZJPLU9hnfYBpOyMVAnkcDB5KZP19nTKARVwt3hFmb13ABIUjWTg/hR5INnw==";
const char privkey_base64[] = "pah9ozKWIjkHCXH3TDsticSPTEnoD/PghEIg97ngZwY=";
#endif


uint8_t *parkpass_crypto_local_public_raw() {
    return m_local_key_public_raw;
}


uint8_t *parkpass_crypto_remote_public_raw() {
    return m_external_key_public_raw;
}


ret_code_t parkpass_crypto_keys_generate() {
ret_code_t err_code;
    
#if USE_DEBUG_KEYS == 1
    NRF_LOG_DEBUG("USE_DEBUG_KEYS == 1, Load predefined keys");
    size_t olen = 0;
    int mbed_ret = mbedtls_base64_decode(m_local_key_public_raw, sizeof(m_local_key_public_raw), &olen, pubkey_base64, sizeof(pubkey_base64) - 1);
    NRF_LOG_DEBUG("Pubkey size: %d bytes, ret: %d", olen, mbed_ret);

    mbed_ret = mbedtls_base64_decode(m_local_key_private_raw, sizeof(m_local_key_private_raw), &olen, privkey_base64, sizeof(privkey_base64) - 1);
    NRF_LOG_DEBUG("Privkey size: %d bytes, ret: %d", olen, mbed_ret);
    
    err_code = nrf_crypto_ecc_public_key_from_raw(  &g_nrf_crypto_ecc_secp256r1_curve_info, 
                                                    &m_local_key_public, 
                                                    m_local_key_public_raw, 
                                                    ECDH_PUBLIC_KEY_SIZE);
    if (err_code != NRF_SUCCESS) {
        NRF_LOG_ERROR("CANNOT load static PUBKEY");
    }

    err_code = nrf_crypto_ecc_private_key_from_raw( &g_nrf_crypto_ecc_secp256r1_curve_info, 
                                                    &m_local_key_private, 
                                                    m_local_key_private_raw, 
                                                    ECDH_PRIVATE_KEY_SIZE);
    if (err_code != NRF_SUCCESS) {
        NRF_LOG_ERROR("CANNOT load static PRIVKEY");
    }
    
    memset(m_shared_key, 0, sizeof(m_shared_key));

#else
    NRF_LOG_DEBUG("Generate Keys");
    err_code = nrf_crypto_ecc_key_pair_generate(NULL,
                                                &g_nrf_crypto_ecc_secp256r1_curve_info,
                                                &m_local_key_private,
                                                &m_local_key_public);
    if (err_code != NRF_SUCCESS) {
        CRYPTO_ERROR_CHECK(err_code);
        return err_code;
    }
    
size_t size;
    size = sizeof(m_local_key_public_raw);
    err_code = nrf_crypto_ecc_public_key_to_raw(&m_local_key_public, m_local_key_public_raw, &size);
    if (err_code != NRF_SUCCESS) {
        CRYPTO_ERROR_CHECK(err_code);
        return err_code;
    }
    
    size = sizeof(m_local_key_private_raw);
    err_code = nrf_crypto_ecc_private_key_to_raw(&m_local_key_private, m_local_key_private_raw, &size);
    if (err_code != NRF_SUCCESS) {
        CRYPTO_ERROR_CHECK(err_code);
        return err_code;
    }
    memset(m_shared_key, 0, sizeof(m_shared_key));

#endif
    NRF_LOG_DEBUG("Keys generated successfull");
    return err_code;
}


ret_code_t parkpass_crypto_keys_calculate_shared() {
ret_code_t err_code = NRF_SUCCESS;
    
    err_code = nrf_crypto_ecc_public_key_from_raw(&g_nrf_crypto_ecc_secp256r1_curve_info,
                                                  &m_external_key_public,
                                                  m_external_key_public_raw,
                                                  ECDH_PUBLIC_KEY_SIZE);
    if (err_code != NRF_SUCCESS) {
        CRYPTO_ERROR_CHECK(err_code);
        return err_code;
    }
    
    size_t size = sizeof(m_shared_key);
    err_code = nrf_crypto_ecdh_compute(NULL,
                                       &m_local_key_private,
                                       &m_external_key_public,
                                       m_shared_key,
                                       &size);
    if (err_code != NRF_SUCCESS) {
        CRYPTO_ERROR_CHECK(err_code);
        return err_code;
    }
    return err_code;
}


ret_code_t parkpass_crypto_keys_free() {

    memset(m_external_key_public_raw, 0, sizeof(m_external_key_public_raw));
    nrf_crypto_ecc_public_key_from_raw(&g_nrf_crypto_ecc_secp256r1_curve_info,
                                       &m_external_key_public,
                                       m_external_key_public_raw,
                                       ECDH_PUBLIC_KEY_SIZE);
    nrf_crypto_ecc_public_key_free(&m_external_key_public);

    memset(m_local_key_public_raw, 0, sizeof(m_local_key_public_raw));
    nrf_crypto_ecc_public_key_from_raw(&g_nrf_crypto_ecc_secp256r1_curve_info, 
                                       &m_local_key_public,
                                       m_local_key_public_raw,
                                       ECDH_PUBLIC_KEY_SIZE);
    nrf_crypto_ecc_public_key_free(&m_local_key_public);
    
    nrf_crypto_ecc_private_key_from_raw(&g_nrf_crypto_ecc_secp256r1_curve_info,
                                        &m_local_key_private,
                                        m_local_key_public_raw,
                                        ECDH_PRIVATE_KEY_SIZE);
    nrf_crypto_ecc_private_key_free(&m_local_key_private);

    memset(m_shared_key, 0, sizeof(m_shared_key));
    return NRF_SUCCESS;
}


ret_code_t parkpass_encrypt(nrf_crypto_aes_context_t *context, const uint8_t *din, size_t din_size, uint8_t *dout, size_t *dout_size) {
ret_code_t  err_code;
    
    err_code = nrf_crypto_aes_init(context,
                                    &g_nrf_crypto_aes_cbc_256_pad_pkcs7_info,
                                    NRF_CRYPTO_ENCRYPT);
    if (err_code != NRF_SUCCESS)
        return err_code;
        
    err_code = nrf_crypto_aes_key_set(context, m_shared_key);
    if (err_code != NRF_SUCCESS)
        return err_code;
    
uint8_t iv[32];
    memset(iv, 0, sizeof(iv));

    err_code = nrf_crypto_aes_iv_set(context, iv);
    if (err_code != NRF_SUCCESS)
        return err_code;
    

size_t len_in;
    
    err_code = nrf_crypto_aes_finalize( context, 
                                        din,
                                        din_size,
                                        dout,
                                        dout_size);
    return err_code;
}


ret_code_t parkpass_decrypt(nrf_crypto_aes_context_t *context, const uint8_t *din, size_t din_size, uint8_t *dout, size_t *dout_size) {
ret_code_t err_code;
    
    err_code = nrf_crypto_aes_init(context,
                                    &g_nrf_crypto_aes_cbc_256_pad_pkcs7_info,
                                    NRF_CRYPTO_DECRYPT);
    if (err_code != NRF_SUCCESS)
        return err_code;

    err_code = nrf_crypto_aes_key_set(context, m_shared_key);
    if (err_code != NRF_SUCCESS)
        return err_code;

uint8_t iv[32];
    memset(iv, 0, sizeof(iv));

    err_code = nrf_crypto_aes_iv_set(context, iv);
    if (err_code != NRF_SUCCESS)
        return err_code;
    
    err_code = nrf_crypto_aes_finalize(context, din, din_size, dout, dout_size);
    return err_code;
}


#define BASE64_SIZE             89
static uint8_t base64_encoded[BASE64_SIZE];


int parkpass_crypto_print_keys() {  
    size_t olen = 0;
    int ret_code = mbedtls_base64_encode(base64_encoded, sizeof(base64_encoded), &olen, m_local_key_public_raw, ECDH_PUBLIC_KEY_SIZE);
    if (ret_code != 0) {
        NRF_LOG_ERROR("Cannot encode PUBKEY");
    } else {
        NRF_LOG_DEBUG("PUBKEY (%d bytes): %s", olen, base64_encoded);
    }

    ret_code = mbedtls_base64_encode(base64_encoded, sizeof(base64_encoded), &olen, m_local_key_private_raw, ECDH_PRIVATE_KEY_SIZE);
    if (ret_code != 0) {
        NRF_LOG_ERROR("Cannon encode PRIVKEY");
    } else {
        NRF_LOG_DEBUG("PRIVKEY (%d bytes): %s", olen, base64_encoded);
    }

    ret_code = mbedtls_base64_encode(base64_encoded, sizeof(base64_encoded), &olen, m_shared_key, ECDH_SHARED_KEY_SIZE);
    if (ret_code != 0) {
        NRF_LOG_ERROR("Cannot encode SHARED key");
    } else {
        NRF_LOG_DEBUG("SHAREDKEY (%d bytes): %s", olen, base64_encoded);
    }
    return 0;
}
