#ifndef _PARKPASS_CRYPTO_H_
#define _PARKPASS_CRYPTO_H_


#include "nrf_crypto.h"
#include "nrf_crypto_ecc.h"
#include "nrf_crypto_ecdh.h"
#include "nrf_crypto_error.h"


#define ECDH_PUBLIC_KEY_SIZE   64
#define ECDH_PRIVATE_KEY_SIZE  32
#define ECDH_SHARED_KEY_SIZE   32


/**
 * \brief ���������� ��������� �� ���������� ����� � �������� ������� ������ 
 *
 * \return ��������� �� ��������� ����� � �������� ������ ����������
 */
uint8_t *parkpass_crypto_local_public_raw();


/**
 * \brief ���������� ��������� �� ���������� ����� � �������� ������ ����������
 *
 * \return ��������� �� ��������� ����� � �������� ������ ����������
 */
uint8_t *parkpass_crypto_remote_public_raw();


/**
 * \brief ���������� ���� ��������� ������
 * \return 
 */
ret_code_t parkpass_crypto_keys_generate();

/**
 * \brief ������� ���� ��������� ������
 * \return NRF_SUCCESS � ����� ������
 */
ret_code_t parkpass_crypto_keys_free();

/**
 * \brief                       ��������� ����� ��������� ����
 * 
 * \return                      
 */
ret_code_t parkpass_crypto_keys_calculate_shared();

ret_code_t parkpass_encrypt(nrf_crypto_aes_context_t *context, const uint8_t *din, size_t din_size, uint8_t *dout, size_t *dout_size);
ret_code_t parkpass_decrypt(nrf_crypto_aes_context_t *context, const uint8_t *din, size_t din_size, uint8_t *dout, size_t *dout_size);


int parkpass_crypto_print_keys();

#endif // _PARKPASS_CRYPTO_H_
