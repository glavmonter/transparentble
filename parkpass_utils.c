#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <nordic_common.h>

#include "parkpass_utils.h"
#include "nrf_log.h"
#include "mbedtls/base64.h"


bool check_valid_INKAS_BLE(const char *data, size_t max_len, uint8_t *pubkey, size_t *pubkey_size, size_t pubkey_size_max) {
const char header[] = "INKAS:";
char *ss;
    if (data == NULL || max_len == 0 || pubkey == NULL || pubkey_size == NULL || pubkey_size_max == 0) {
        return false;
    }

    ss = strnstr(data, header, max_len);
    if (ss != data) {
        // ��� ��������� INKAS
        return false;
    }
    
    // ���� ���������� �
    char *key_start = data + sizeof(header) - 1;

    size_t olen = 0;
    int ret = mbedtls_base64_decode(NULL, 0, &olen, key_start, strnlen(key_start, 128));
    NRF_LOG_DEBUG("Key buffer size = %d", olen);

    if (ret == MBEDTLS_ERR_BASE64_INVALID_CHARACTER) {
        return false;
    }

    if (olen > pubkey_size_max) {
        NRF_LOG_DEBUG("pubkey_size_max(%d) to small", pubkey_size_max);
        return false;
    }

    ret = mbedtls_base64_decode(pubkey, pubkey_size_max, &olen, key_start, strnlen(key_start, 128));
    if (ret == 0) {
        NRF_LOG_DEBUG("Base64 key decoded successfully");
        NRF_LOG_DEBUG("Return True");

        *pubkey_size = olen;
        return true;

    } else if (ret == MBEDTLS_ERR_BASE64_BUFFER_TOO_SMALL) {
        NRF_LOG_DEBUG("Base64 decode error: MBEDTLS_ERR_BASE64_BUFFER_TOO_SMALL");
    } else if (ret == MBEDTLS_ERR_BASE64_INVALID_CHARACTER) {
        NRF_LOG_DEBUG("Base64 decode error: MBEDTLS_ERR_BASE64_INVALID_CHARACTER");
    }    

    return false;
}


bool check_valid_OLLEH(const char *data, size_t max_len, uint32_t *arg1, uint8_t *key, size_t *key_size, size_t key_size_max) {
const char header[] = "OLLEH:";
char *ss;

    if (data == NULL || max_len == 0 || arg1 == NULL || key == NULL || key_size == NULL || key_size_max == 0) {
        return false;
    }

    ss = strnstr(data, header, max_len);
    if (ss != data) {
        // header not found
        NRF_LOG_DEBUG("Header not found");
        *key_size = 0;
        *arg1 = 0;
        return false;
    }

    const char *arg1_address = data + sizeof(header) - 1;
    
    // ���� ������ ��������� $ � ������
    char *arg1_end = strnstr(data, "$", max_len);
    if (arg1_end == NULL) {
        // ��� ���������
        NRF_LOG_DEBUG("$ not found");
        *key_size = 0;
        *arg1 = 0;
        return false;
    }
    
    // ss ��������� �� $. ����� ���� ������ ���� ������ �����
    size_t digit_len = arg1_end - arg1_address;
    NRF_LOG_DEBUG("Digit len: %d", digit_len);
    if (digit_len == 0) {
        // ��� ������ � ���������
        NRF_LOG_DEBUG("No data in Argument");
        *key_size = 0;
        *arg1 = 0;
        return false;
    }

    // ���������, ��� ��� �������� ������: ����� ������� ����
    if (*arg1_address == '-') {
        NRF_LOG_DEBUG("Argument is negative");
        *key_size = 0;
        *arg1 = 0;
        return false;
    }

bool n_valid = false;
    char *d = arg1_address;
    while (d != arg1_end) {
        n_valid |= !isdigit(*d);
        d++;
    }
    
    NRF_LOG_DEBUG("n_valid: %s", n_valid ? "True" : "False");
    if (n_valid == true) {
        NRF_LOG_DEBUG("Argument representative is invalid");
        *key_size = 0;
        *arg1 = 0;
        return false;
    }
    
    char *end;
    long int arg_ll = strtol(arg1_address, &end, 10);
    NRF_LOG_DEBUG("Argument: %d", arg_ll);
    if (end != arg1_end) {
        NRF_LOG_DEBUG("Argument parse error");
        *key_size = 0;
        *arg1 = 0;
        return false;
    }
    
    char *key_start = arg1_end + 1;
    NRF_LOG_DEBUG("Key: %s", key_start);
    
    // Check base64 decoded size
    size_t olen = 0;
    int b_ret = mbedtls_base64_decode(NULL, 0, &olen, key_start, strnlen(key_start, 128));
    NRF_LOG_DEBUG("Key buffer size = %d", olen);
    
    if (b_ret == MBEDTLS_ERR_BASE64_INVALID_CHARACTER) {
        return false;
    }

    if (olen > key_size_max) {
        NRF_LOG_DEBUG("key_size_max(%d) to small", key_size_max);
        return false;
    }

    b_ret = mbedtls_base64_decode(key, key_size_max, &olen, key_start, strnlen(key_start, 128));
    if (b_ret == 0) {
        NRF_LOG_DEBUG("Base64 key decoded successfully");
        NRF_LOG_DEBUG("Return True");
        
        *arg1 = arg_ll;
        *key_size = olen;
        return true;

    } else if (b_ret == MBEDTLS_ERR_BASE64_BUFFER_TOO_SMALL) {
        NRF_LOG_DEBUG("Base64 decode error: MBEDTLS_ERR_BASE64_BUFFER_TOO_SMALL");
    } else if (b_ret == MBEDTLS_ERR_BASE64_INVALID_CHARACTER) {
        NRF_LOG_DEBUG("Base64 decode error: MBEDTLS_ERR_BASE64_INVALID_CHARACTER");
    }
    return false;
}


bool check_valid_GETPC(const char *data, size_t max_len, uint32_t *dataout) {
const char msg_getpc[] = "GETPC:";
char *ss = strnstr(data, msg_getpc, max_len);
    if (ss != data)
        return false;
    
char *start_symbol = data + strlen(msg_getpc);
 
bool n_valid = false;
    while ((*start_symbol != '\0') && (max_len > 0)) {
        n_valid |= !isdigit(*start_symbol);
        start_symbol++;
        max_len--;
    }
    
    if (max_len == 0)
        return false;

    if (!n_valid == true) {
        // ��������� ������ � �����
        long int ll;
        ll = strtol(data + strlen(msg_getpc), NULL, 10);
        if (ll > 0) {
            *dataout = ll;
            return true;
        }
        return false;
    }
    return false;
}


bool check_valid_ID(const char *data, size_t max_len, int32_t *parking_id) {
UNUSED_PARAMETER(max_len);
int arg = 0;
    int ret = sscanf(data, "ID:%d", &arg);
    if (ret == 1) {
        *parking_id = arg;
        return true;
    }
    return false;
}


bool check_valid_THRES(const char *data, size_t max_len, int8_t *rssi_threshold) {
UNUSED_PARAMETER(max_len);

int threshold = 0;
    int ret = sscanf(data, "THRES:%d", &threshold);
    if ((ret == 1) && (threshold >= -100) && (threshold < 0)) {
        *rssi_threshold = threshold;
        return true;
    }
    return false;
}

bool check_valid_DELTA(const char *data, size_t max_len, int8_t *rssi_delta) {
int delta = 0;
    int ret = sscanf(data, "DELTA:%d", &delta);
    if ((ret == 1) && (delta > 0) && (delta < 50)) {
        *rssi_delta = delta;
        return true;
    }
    return false;
}

bool check_valid_BLETO(const char *data, size_t max_len, uint32_t *ble_timeout_ms) {
int timeout = 0;
    int ret = sscanf(data, "BLETO:%d", &timeout);
    if ((ret == 1) && (timeout > 20) && (timeout < 1000 * 60)) {
        *ble_timeout_ms = timeout;
        return true;
    }
    return false;
}


bool check_valid_DT(const char *data, size_t max_len, time_t *unixtime) {
time_t tt = 0;
    int ret = sscanf(data, "DT:%d", &tt);
    if (ret == 1) {
        *unixtime = tt;
        return true;
    }
    return false;
}


bool check_valid_DFU(const char *data, size_t max_len) {
    int ret = strncmp(data, "DFU\n", max_len);
    return (ret == 0);
}

bool check_valid_PING(const char *data, size_t max_len) {
    int ret = strncmp(data, "PING\n", max_len);
    return (ret == 0);
}

bool check_valid_RSSI(const char *data, size_t max_len) {
    int ret = strncmp(data, "RSSI\n", max_len);
    return (ret == 0);
}


bool check_valid_INKAS(const char *data, size_t max_len) {
    int ret = strncmp(data, "INKAS\n", max_len);
    return (ret == 0);
}



uint8_t tallymarker_hextobin(const char * str, uint8_t * bytes, size_t blen) {
   uint8_t  pos;
   uint8_t  idx0;
   uint8_t  idx1;

   // mapping of ASCII characters to hex values
   const uint8_t hashmap[] =
   {
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // ........
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // ........
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // ........
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // ........
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //  !"#$%&'
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // ()*+,-./
     0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, // 01234567
     0x08, 0x09, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 89:;<=>?
     0x00, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x00, // @ABCDEFG
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // HIJKLMNO
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // PQRSTUVW
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // XYZ[\]^_
     0x00, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x00, // `abcdefg
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // hijklmno
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // pqrstuvw
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // xyz{|}~.
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // ........
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // ........
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // ........
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // ........
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // ........
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // ........
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // ........
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // ........
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // ........
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // ........
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // ........
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // ........
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // ........
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // ........
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // ........
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00  // ........
   };
   
   memset(bytes, 0, blen);
   for (pos = 0; ((pos < (blen*2)) && (pos < strlen(str))); pos += 2)
   {
      idx0 = (uint8_t)str[pos+0];
      idx1 = (uint8_t)str[pos+1];
      bytes[pos/2] = (uint8_t)(hashmap[idx0] << 4) | hashmap[idx1];
   };

   return(0);
}


static void print_array(uint8_t const * p_string, size_t size)
{
    #if NRF_LOG_ENABLED
    size_t i;
    NRF_LOG_RAW_INFO("    ");
    for(i = 0; i < size; i++)
    {
        NRF_LOG_RAW_INFO("%02x", p_string[i]);
    }
    #endif // NRF_LOG_ENABLED
}


void print_hex(char const * p_msg, uint8_t const * p_data, size_t size)
{
    NRF_LOG_INFO(p_msg);
    print_array(p_data, size);
    NRF_LOG_RAW_INFO("\r\n");
}

