#ifndef _PARKPASS_UTILS_H_
#define _PARKPASS_UTILS_H_

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include <time.h>


/**
 * \brief                   �������� ���������� ������ OLLEH �� ������� 'OLLEH:arg1$key\0', ���
 *                          arg1 - int, ����� ����, ������� ����� ��������;
 *                          key - Base64 ������������� �������� ���� ���������� ��������
 * 
 * \param data[in]          ��������� �� ������, ������� ���������
 * \param max_len[in]       ������������ ����� ������ data
 * \param arg1[out]         ��������� �� ������������ ��������
 * \param key[out]          ��������� �� �����, � ������� ����� ����������� ����
 * \param key_size[out]     ���������� ���� ��������� � ����� key
 * \param key_size_max[in]  ������������ ������ ������ key
 * 
 * \return true ���� ������ ������� ��� false ��� �������
*/
bool check_valid_OLLEH(const char *data, size_t max_len, uint32_t *arg1, uint8_t *key, size_t *key_size, size_t key_size_max);
bool check_valid_INKAS_BLE(const char *data, size_t max_len, uint8_t *pubkey, size_t *pubkey_size, size_t pubkey_size_max);

bool check_valid_GETPC(const char *data, size_t max_len, uint32_t *dataout);

bool check_valid_THRES(const char *data, size_t max_len, int8_t *rssi_threshold);
bool check_valid_DELTA(const char *data, size_t max_len, int8_t *rssi_delta);
bool check_valid_BLETO(const char *data, size_t max_len, uint32_t *ble_timeout_ms);
bool check_valid_DT(const char *data, size_t max_len, time_t *unixtime);
bool check_valid_DFU(const char *data, size_t max_len);
bool check_valid_PING(const char *data, size_t max_len);
bool check_valid_RSSI(const char *data, size_t max_len);

bool check_valid_ID(const char *data, size_t max_len, int32_t *parking_id);
bool check_valid_INKAS(const char *data, size_t max_len);
bool check_valid_INKAS_BLE(const char *data, size_t max_len, uint8_t *pub_key, size_t *key_size, size_t key_size_max);

uint8_t tallymarker_hextobin(const char * str, uint8_t * bytes, size_t blen);
void print_hex(char const * p_msg, uint8_t const * p_data, size_t size);



#define CRYPTO_ERROR_CHECK(error)     \
do                                  \
{                                   \
    if (error != NRF_SUCCESS)       \
    {                               \
        NRF_LOG_ERROR("Error 0x%04X: %s", error, nrf_crypto_error_string_get(error));\
        APP_ERROR_CHECK(error);     \
    }                               \
} while(0)


#endif // _PARKPASS_UTILS_H_
