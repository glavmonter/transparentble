#include <nrf_error.h>
#include <sdk_errors.h>
#include <stdint.h>

#include "ble_types.h"
#include "sdk_config.h"
#include "sdk_errors.h"
#include "app_timer.h"

#include "rssi_calculator.h"
#include "transparent.h"
#include "parkpass_config.h"

#define RSSI_CALC_LEN   8  /// ���������� ����������

#define NRF_LOG_MODULE_NAME rssi_calc
#define NRF_LOG_LEVEL       4   // Debug
#define NRF_LOG_INFO_COLOR  2   // Red
#define NRF_LOG_DEBUG_COLOR 2   // Red

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
NRF_LOG_MODULE_REGISTER();


static int8_t m_rssi_threshold = RSSI_CALC_THRESHOLD;
static int8_t m_rssi_delta = RSSI_CALC_DELTA;

typedef struct _RssiCalc {
    uint16_t conn_handle;
    int8_t   rssi_array[RSSI_CALC_LEN];
    uint8_t  current_index;
    int16_t  rssi;
    uint32_t time_added;
} RssiCalc;


extern __IO uint32_t m_iSysTickCnt;

static __IO RssiCalc m_RssiCalc[NRF_SDH_BLE_PERIPHERAL_LINK_COUNT];
ret_code_t RssiCalcGetActiveConnections(uint16_t *conn_handles, uint16_t *len);
static int8_t RssiCalcGetLastRssi(uint16_t conn_haldle);

static bool isClossedDevice(uint16_t conn_handle, uint32_t *pDelta);

APP_TIMER_DEF(m_rssi_timer_id);
static void rssi_timer_callback(void *p_context) {
UNUSED_PARAMETER(p_context);
uint16_t handles[NRF_SDH_BLE_PERIPHERAL_LINK_COUNT];
uint16_t len;

#if 1
// TODO �������� �� ���������� �����������
    RssiCalcGetActiveConnections(handles, &len);
    for (uint8_t i = 0; i < len; ++i) {
        int8_t last_rssi = RssiCalcGetLastRssi(handles[i]);
        if (last_rssi != RSSI_INVALID)
            RssiCalcUpdate(handles[i], last_rssi);
    }
#endif

uint16_t nearest;
ret_code_t err_code = RssiCalcGetNearestConnection(&nearest, handles, &len);
    if (err_code == NRF_SUCCESS) {
        uint32_t delta;
        // TODO ��� ��� ������ ParkPass
        //bool is_clossed = isClossedDevice(nearest, &delta);
        //parkpass_start(nearest, handles, len, is_clossed, delta);
    }
}


static uint8_t local_str[6 + 8 * NRF_SDH_BLE_PERIPHERAL_LINK_COUNT + 4];

APP_TIMER_DEF(m_rssi_show_timer_id);
static void rssi_show_timer_callback(void *p_context) {
UNUSED_PARAMETER(p_context);
uint16_t handles[NRF_SDH_BLE_PERIPHERAL_LINK_COUNT];
uint16_t len;

    RssiCalcGetActiveConnections(handles, &len);
    if (len > 0) {
        uint8_t *pos = local_str;
        int written = 0;
        for (uint32_t i = 0; i < len; i++) {
            written = 0;
            sprintf(pos, "(%02d:%02d)%n", handles[i], RssiCalcGetRssi(handles[i]), &written);
            pos += written;
        }
        *pos = '\0';
        NRF_LOG_INFO("RSSI: %s", local_str);
    }
    
uint16_t nearest;
ret_code_t err_code = RssiCalcGetNearestConnection(&nearest, handles, &len);
    if (err_code == NRF_SUCCESS) {
        NRF_LOG_INFO("Nearest: [%d], RSSI: %d dBm", nearest, RssiCalcGetRssi(nearest));
        for (uint32_t i = 0; i < len; i++) {
            NRF_LOG_INFO("Faraway[%d]: %d dBm%s", handles[i], RssiCalcGetRssi(handles[i]), i == len - 1 ? "\n" : "");
        }
        
        // TODO ��� ��� ������ ParkPass
        //uint32_t delta;
        //bool is_clossed = isClossedDevice(nearest, &delta);
        //parkpass_start(nearest, handles, len, is_clossed, delta);
    }
}


static bool isClossedDevice(uint16_t conn_handle, uint32_t *pDelta) {
    if ((conn_handle == BLE_CONN_HANDLE_INVALID) || (conn_handle == BLE_CONN_HANDLE_ALL))
        return false;
    
    for (uint32_t i = 0; i < NRF_SDH_BLE_PERIPHERAL_LINK_COUNT; i++) {
        if (m_RssiCalc[i].conn_handle == conn_handle) {
            uint32_t delta = m_iSysTickCnt;
            delta = delta - m_RssiCalc[i].time_added;
            *pDelta = delta;
            if (delta > PP_CLOSSED_DEVICE_TIMEOUT) {
                return false;
            } else {
                return true;
            }
        }
    }
    return false;
}


size_t GetAllRSSI(uint8_t *buf, size_t max_len) {
uint16_t handles[NRF_SDH_BLE_PERIPHERAL_LINK_COUNT];
uint16_t len;
    int all_size = 0;
    RssiCalcGetActiveConnections(handles, &len);
    if (len > 0) {
        NRF_LOG_DEBUG("LEN: %d", len);
        int written;
        sprintf(buf, "RSSI:%n", &written);
        uint8_t *pos = buf + written;
        for (uint32_t i = 0; i < len; i++) {
            written = 0;
            sprintf(pos, "(%02d:%02d)%n", handles[i], RssiCalcGetRssi(handles[i]), &written);
            pos += written;
            all_size += written;
        }
        *pos = '\n';
        pos++;
        *pos = '\0';
        NRF_LOG_DEBUG("BUF: %s", buf);
        return all_size;
    }
    return 0;
}

/**
 * @brief ������������� ���������� �������� ����������� RSSI
 */
void RssiCalcInit() {
    m_rssi_threshold = ppc_get_rssi_threshold();
    m_rssi_delta = ppc_get_rssi_delta();

    NRF_LOG_INFO("Rssi Driver Init");
    NRF_LOG_INFO("RSSI Threshold: %d dBm", m_rssi_threshold);
    NRF_LOG_INFO("RSSI Delta: %d dBm", m_rssi_delta);

    for (uint32_t i = 0; i < NRF_SDH_BLE_PERIPHERAL_LINK_COUNT; i++) {
        memset(m_RssiCalc[i].rssi_array, 0, RSSI_CALC_LEN);
        m_RssiCalc[i].current_index = 0;
        m_RssiCalc[i].conn_handle = BLE_CONN_HANDLE_INVALID;
        m_RssiCalc[i].rssi = RSSI_INVALID;
        m_RssiCalc[i].time_added = 0;
    }

ret_code_t err_code;
    err_code = app_timer_create(&m_rssi_timer_id, APP_TIMER_MODE_REPEATED, rssi_timer_callback);
    APP_ERROR_CHECK(err_code);

    err_code = app_timer_create(&m_rssi_show_timer_id, APP_TIMER_MODE_REPEATED, rssi_show_timer_callback);
    APP_ERROR_CHECK(err_code);

    app_timer_start(m_rssi_timer_id, APP_TIMER_TICKS(150), NULL);
    app_timer_start(m_rssi_show_timer_id, APP_TIMER_TICKS(2500), NULL);
}


/**
 * @brief ��������� ����������� �� ��������� RSSI
 * @param[in] conn_handle ����� �����������
 * @return ����������
 *     - @ref NRF_ERROR_INVALID_PARAM conn_handle �� ������ ��� ������������ ���������� ����������� ����������
 *     - @ref NRF_SUCCESS �� ������
 */
ret_code_t RssiCalcConnectionAdd(uint16_t conn_handle) {
    if ((conn_handle == BLE_CONN_HANDLE_INVALID) || (conn_handle == BLE_CONN_HANDLE_ALL))
        return NRF_ERROR_INVALID_PARAM;

    for (uint32_t i = 0; i < NRF_SDH_BLE_PERIPHERAL_LINK_COUNT; i++) {
        if (m_RssiCalc[i].conn_handle == conn_handle)
            return NRF_ERROR_INVALID_PARAM;

        if (m_RssiCalc[i].conn_handle == BLE_CONN_HANDLE_INVALID) {
            memset(m_RssiCalc[i].rssi_array, 0, RSSI_CALC_LEN);
            m_RssiCalc[i].current_index = 0;
            m_RssiCalc[i].rssi = RSSI_INVALID;
            m_RssiCalc[i].conn_handle = conn_handle;
            m_RssiCalc[i].time_added = m_iSysTickCnt;

            NRF_LOG_INFO("Connection Added: %d, time: %u", conn_handle, m_RssiCalc[i].time_added);
            return NRF_SUCCESS;
        }
    }

    return NRF_ERROR_INVALID_PARAM;
}


/**
 * @brief ������� ����������� �� ��������������
 * @param[in] conn_handle ����� �����������
 * @return ���������� ��������
 *     - @ref NRF_ERROR_NOT_FOUND ���� conn_handle �� ������ � ������������������ �����������
 *     - @ref NRF_SUCCESS �� ������
 */
ret_code_t RssiCalcConnectionRemove(uint16_t conn_handle) {
    if ((conn_handle == BLE_CONN_HANDLE_INVALID) || (conn_handle == BLE_CONN_HANDLE_ALL))
        return NRF_ERROR_NOT_FOUND;

    for (uint32_t i = 0; i < NRF_SDH_BLE_PERIPHERAL_LINK_COUNT; i++) {
        if (m_RssiCalc[i].conn_handle == conn_handle) {
            m_RssiCalc[i].conn_handle = BLE_CONN_HANDLE_INVALID;
            m_RssiCalc[i].time_added = 0;

            NRF_LOG_INFO("Connection Removed: %d", conn_handle);
            return NRF_SUCCESS;
        }
    }

    return NRF_ERROR_NOT_FOUND;
}


/**
 * @brief ��������� �������� RSSI ��� ������� � ����������� ���������� �������
 * @param[in] conn_handle ����� �����������
 * @param[in] rssi �������� RSSI
 * @return ���������� ����������
 *     - @ref NRF_ERROR_NOT_FOUND conn_handle �� ������ � �������������� ������������
 *     - @ref NRF_SUCCESS ��� ������
 */
ret_code_t RssiCalcUpdate(uint16_t conn_handle, int8_t rssi) {
    if ((conn_handle == BLE_CONN_HANDLE_INVALID) || (conn_handle == BLE_CONN_HANDLE_ALL))
        return NRF_ERROR_NOT_FOUND;

    for (uint32_t i = 0; i < NRF_SDH_BLE_PERIPHERAL_LINK_COUNT; i++) {
        if (m_RssiCalc[i].conn_handle == conn_handle) {
            m_RssiCalc[i].rssi = 0;

            for (uint32_t r = 1; r < RSSI_CALC_LEN; r++) {
                m_RssiCalc[i].rssi_array[r - 1] = m_RssiCalc[i].rssi_array[r];
                m_RssiCalc[i].rssi += m_RssiCalc[i].rssi_array[r - 1]; 
            }

            m_RssiCalc[i].rssi_array[RSSI_CALC_LEN - 1] = rssi;
            m_RssiCalc[i].rssi += m_RssiCalc[i].rssi_array[RSSI_CALC_LEN - 1]; 
            m_RssiCalc[i].rssi /= RSSI_CALC_LEN;

            if (m_RssiCalc[i].current_index < RSSI_CALC_LEN)
                m_RssiCalc[i].current_index++;
            
            return NRF_SUCCESS;
        }
    }

    return NRF_ERROR_NOT_FOUND;
}


/**
 * @brief ��������� ������ �������� �����������, ��� ������� ���� ������� RSSI
 * @param[out] conn_handles ��������� �� ������ ������������ �����������, ������ �� ����� @ref NRF_SDH_BLE_PERIPHERAL_LINK_COUNT
 * @param[out] len ��������� �� ���������� �����������
 * @return NRF_SUCCESS ������
 */
ret_code_t RssiCalcGetActiveConnections(uint16_t *conn_handles, uint16_t *len) {
    *len = 0;
    for (uint32_t i = 0; i < NRF_SDH_BLE_PERIPHERAL_LINK_COUNT; i++) {
        if (m_RssiCalc[i].conn_handle != BLE_CONN_HANDLE_INVALID) {
            *conn_handles = m_RssiCalc[i].conn_handle;
            conn_handles++;
            *len += 1;
        }
    }
    return NRF_SUCCESS;
}

void PrintActiveConnections() {
uint16_t handles[NRF_SDH_BLE_PERIPHERAL_LINK_COUNT];
uint16_t len;
    RssiCalcGetActiveConnections(handles, &len);
    NRF_LOG_DEBUG("Active connections (%d): ", len);
    for (uint16_t i = 0; i < len; i++)
        NRF_LOG_DEBUG("conn: %d", handles[i]);
}



static int8_t RssiCalcGetLastRssi(uint16_t conn_haldle) {
    if ((conn_haldle == BLE_CONN_HANDLE_INVALID) || (conn_haldle == BLE_CONN_HANDLE_ALL))
        return RSSI_INVALID;
    
    for (uint32_t i = 0; i < NRF_SDH_BLE_PERIPHERAL_LINK_COUNT; ++i) {
        if (m_RssiCalc[i].conn_handle == conn_haldle)
            return m_RssiCalc[i].rssi_array[RSSI_CALC_LEN - 1];
    }
    return RSSI_INVALID;
}

/**
 * @brief ������� ��������� ������������ �������� RSSI
 * @param[in] conn_handle ����� �����������
 * @return �������� RSSI ��� @ref RSSI_INVALID � ������ �������
 */
int8_t RssiCalcGetRssi(uint16_t conn_handle) {
    if ((conn_handle == BLE_CONN_HANDLE_INVALID) || (conn_handle == BLE_CONN_HANDLE_ALL))
        return RSSI_INVALID;

    for (uint32_t i = 0; i < NRF_SDH_BLE_PERIPHERAL_LINK_COUNT; i++) {
        if (m_RssiCalc[i].conn_handle == conn_handle)
            return m_RssiCalc[i].current_index == RSSI_CALC_LEN ? m_RssiCalc[i].rssi : INT8_MAX;
    }
    return RSSI_INVALID;
}


typedef struct {
    uint16_t conn_handle[NRF_SDH_BLE_PERIPHERAL_LINK_COUNT];
    int8_t rssi[NRF_SDH_BLE_PERIPHERAL_LINK_COUNT];
    uint32_t size;
} conn;


void swap(conn *c, uint32_t a, uint32_t b) {
uint16_t tmp_handle = c->conn_handle[a];
int8_t tmp_rssi = c->rssi[a];
    
    c->conn_handle[a] = c->conn_handle[b];
    c->rssi[a] = c->rssi[b];
    c->conn_handle[b] = tmp_handle;
    c->rssi[b] = tmp_rssi;
}

void conn_sort(conn *connections) {
    if (connections->size == 0)
        return;

    for (uint16_t i = 0; i < connections->size - 1; i++) {
        for (uint16_t j = 0; j < connections->size - i - 1; j++) {
            if (connections->rssi[j] < connections->rssi[j + 1])
                swap(connections, j, j + 1);
        }
    }
}

//void conn_filter(conn *in_conns, uint16_t in_n)
/**
 * @brief ����� ��������� ����������� (max(RSSI > @ref RSSI_CALC_THRESHOLD) dBm � �� ���������� �� ����� @ref RSSI_CALC_DELTA dbm)
 * @param[out] conn_nearest ��������� �� ����� ����������� ����������
 * @param[out] conns_faraway ��������� �� ������ ������� �����������
 * @param[out] faraway_len ��������� �� ���������� ������� ������������
 * @return ���������� ����������:
 *     - @ref NRF_SUCCESS ��������� ������
 *     - @ref NRF_ERROR_NOT_FOUND ��������� �� �������
 */
ret_code_t RssiCalcGetNearestConnection(uint16_t *conn_nearest, uint16_t *conns_faraway, uint16_t *faraway_len) {
uint8_t c_valid = 0;
uint16_t handles[NRF_SDH_BLE_PERIPHERAL_LINK_COUNT];
uint16_t handles_len;

conn c;
    c.size = 0;
    
    *faraway_len = 0;
    RssiCalcGetActiveConnections(handles, &handles_len);
    if (handles_len == 0)
        return NRF_ERROR_NOT_FOUND;

    uint8_t cnt = 0;
    for (uint32_t i = 0; i < handles_len; i++) {
        int8_t rssi = RssiCalcGetRssi(handles[i]);
        if (rssi == RSSI_INVALID)
            return NRF_ERROR_NOT_FOUND;
        
        if (rssi > m_rssi_threshold) {
            c.conn_handle[c.size] = handles[i];
            c.rssi[c.size] = rssi;
            c.size++;
        }
    }  

    conn_sort(&c);

    if (c.size == 0)
        return NRF_ERROR_NOT_FOUND;
    
    if (c.size == 1) {
        *conn_nearest = c.conn_handle[0];
        for (uint32_t i = 0; i < handles_len; i++) {
            if (handles[i] != *conn_nearest) {
                conns_faraway[*faraway_len] = handles[i];
                *faraway_len += 1;
            }
        }
        return NRF_SUCCESS;
    }

    // c.size > 1
    // �������� ������ ������� �� ������
    if (c.rssi[0] - c.rssi[1] >= m_rssi_delta) {
        *conn_nearest = c.conn_handle[0];
        
        for (uint32_t i = 0; i < handles_len; i++) {
            if (handles[i] != *conn_nearest) {
                conns_faraway[*faraway_len] = handles[i];
                *faraway_len += 1;
            }
        }
        return NRF_SUCCESS;
    }

    return NRF_ERROR_NOT_FOUND;
}


ret_code_t RssiCalcSetThreshold(int8_t rssi_threshold) {
    NRF_LOG_INFO("Set RSSI Threshold: %d dBm", rssi_threshold);
    m_rssi_threshold = rssi_threshold;
    return NRF_SUCCESS;
}

ret_code_t RssiCalcSetDelta(int8_t delta) {
    NRF_LOG_INFO("Set RSSI Delta: %d dBm", delta);
    m_rssi_delta = delta;
    return NRF_SUCCESS;
}


void DisconnectAll() {
uint16_t handles[NRF_SDH_BLE_PERIPHERAL_LINK_COUNT];
uint16_t len;

    RssiCalcGetActiveConnections(handles, &len);
    for (uint32_t i = 0; i < len; i++) {
        sd_ble_gap_disconnect(handles[i], BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
    }
}


void RssiCalcTest() {
ret_code_t err_code;
    
    err_code = RssiCalcConnectionAdd(1);
    NRF_LOG_DEBUG("Add(1): %s", nrf_strerror_get(err_code));
    
    err_code = RssiCalcConnectionAdd(0);
    NRF_LOG_DEBUG("Add(0): %s", nrf_strerror_get(err_code));

    err_code = RssiCalcConnectionAdd(0);
    NRF_LOG_DEBUG("Add(0): %s", nrf_strerror_get(err_code));
    
    err_code = RssiCalcConnectionAdd(2);
    NRF_LOG_DEBUG("Add(2): %s", nrf_strerror_get(err_code));
    
    err_code = RssiCalcConnectionAdd(3);
    NRF_LOG_DEBUG("Add(3): %s", nrf_strerror_get(err_code));

    err_code = RssiCalcConnectionAdd(4);
    NRF_LOG_DEBUG("Add(4): %s", nrf_strerror_get(err_code));
    
    PrintActiveConnections();

    err_code = RssiCalcConnectionRemove(5);
    NRF_LOG_DEBUG("Remove(5): %s", nrf_strerror_get(err_code));

    err_code = RssiCalcConnectionRemove(1);
    NRF_LOG_DEBUG("Remove(1): %s", nrf_strerror_get(err_code));
    
    err_code = RssiCalcConnectionRemove(1);
    NRF_LOG_DEBUG("Remove(1): %s", nrf_strerror_get(err_code));
    
    PrintActiveConnections();
    
    RssiCalcInit();
    PrintActiveConnections();
    
    err_code = RssiCalcConnectionAdd(0);
    NRF_LOG_DEBUG("Add(0): %s", nrf_strerror_get(err_code));

    NRF_LOG_DEBUG("Rssi: %d", RssiCalcGetRssi(1));

    for (uint32_t i = 0; i < 32; i++) {
        err_code = RssiCalcUpdate(0, -40 + i * 5);
        NRF_LOG_DEBUG("Update(0, %d): %s", -40 + i * 5, nrf_strerror_get(err_code));
        
        NRF_LOG_DEBUG("Rssi: %d", RssiCalcGetRssi(0));
    }


    err_code = RssiCalcUpdate(1, -40);
    NRF_LOG_DEBUG("Update(0, -40): %s", nrf_strerror_get(err_code));
    
    err_code = RssiCalcUpdate(2, -35);
    NRF_LOG_DEBUG("Update(0, -35): %s", nrf_strerror_get(err_code));
    
    NRF_LOG_DEBUG("Rssi(1): %d", RssiCalcGetRssi(2));

    NRF_BREAKPOINT_COND;
}
