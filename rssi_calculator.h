#ifndef _RSSI_CALCULATOR_H_
#define _RSSI_CALCULATOR_H_

#include <stdint.h>
#include "sdk_errors.h"

#ifdef __cplusplus
extern "C" {
#endif


#define RSSI_CALC_THRESHOLD     (-43) /// �������� ������ ��� �����������
#define RSSI_CALC_DELTA         (10)  /// �������� ����������� ������� 
#define RSSI_INVALID            (INT8_MAX)

void RssiCalcInit();
ret_code_t RssiCalcConnectionAdd(uint16_t conn_handle);
ret_code_t RssiCalcConnectionRemove(uint16_t conn_handle);
ret_code_t RssiCalcUpdate(uint16_t conn_handle, int8_t rssi);
ret_code_t RssiCalcUpdateRequest(uint16_t conn_handle);
int8_t RssiCalcGetRssi(uint16_t conn_handle);

ret_code_t RssiCalcGetNearestConnection(uint16_t *conn_nearest, uint16_t *conns_faraway, uint16_t *faraway_len);
ret_code_t RssiCalcSetThreshold(int8_t rssi_threshold);
ret_code_t RssiCalcSetDelta(int8_t delta);

void DisconnectAll();
void RssiCalcTest();
size_t GetAllRSSI(uint8_t *buf, size_t max_len);

#ifdef __cplusplus
} // extern "C"
#endif



#endif // _RSSI_CALCULATOR_H_
