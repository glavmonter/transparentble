#include "app_timer.h"
#include "app_uart.h"
#include "nrf_sdm.h"
#include "app_timer.h"
#include "app_uart.h"
#include "ble_conn_params.h"

#include "nrf_crypto.h"
#include "nrf_crypto_ecc.h"
#include "nrf_crypto_ecdh.h"
#include "nrf_crypto_error.h"
#include "nrf_delay.h"

#include "parkpass_config.h"
#include "transparent.h"
#include "rssi_calculator.h"
#include "nrf_calendar.h"
#include "time.h"

#include "parkpass_crypto.h"
#include "parkpass_utils.h"
#include "mbedtls/base64.h"


#define NRF_LOG_MODULE_NAME transparent
#define NRF_LOG_LEVEL       4   // Debug
#define NRF_LOG_INFO_COLOR  2   // Red
#define NRF_LOG_DEBUG_COLOR 2   // Red

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
NRF_LOG_MODULE_REGISTER();

#define BLE_MAX_DATA_LEN            128
#define AES_BUFFERS_SIZE            256
#define UART_TIMEOUT                2000
#define UART_MAX_DATA_LEN           134

#define CRYPTO (1)
#define INKAS_TIMEOUT               (1000*60*1) // min

BLE_NUS_DEF(m_nus, NRF_SDH_BLE_TOTAL_LINK_COUNT);                                   /**< BLE NUS service instance. */

uint32_t m_ble_timeout = APP_TIMER_TICKS(10000);

static uint8_t m_uart_buffer_rx[UART_MAX_DATA_LEN];
static uint32_t m_uart_buffer_rx_size = 0;
static uint8_t m_uart_buffer_tx[UART_MAX_DATA_LEN];
static uint32_t m_uart_buffer_tx_size = 0;


static uint16_t m_conn_handle = BLE_CONN_HANDLE_INVALID; /**< ��������� ���������� */
static uint16_t m_expectant_conn_handle = BLE_CONN_HANDLE_INVALID; /**< ���������� �� ���������� ����� */

static int32_t m_iBleRemainingBytes = 0;
static int32_t m_iUartRemainingBytes = 0;


// �������
APP_TIMER_DEF(transparent_timer_id);
static void transparent_timer_callback(void *p_context);
APP_TIMER_DEF(timer_reset_id);
static void timer_reset_callback(void *p_context);


static void transparent_uart_tx(const uint8_t *data, size_t len);
static ret_code_t transparent_ble_transmit(uint16_t conn_handle, uint8_t *data, size_t len, uint32_t timeout, bool startTimer, bool crypt);
static ret_code_t ble_send_hello(void);
static ret_code_t uart_received_conn(void);
ret_code_t ble_disconnect(uint8_t status);

static ret_code_t DisconnectAndReset();

// ������ ��� ����������
uint8_t m_pbAESEncryptedBuffer[AES_BUFFERS_SIZE];
size_t m_iAESEncryptedSize = 0;
uint8_t m_pbAESDecryptedBuffer[AES_BUFFERS_SIZE];
size_t m_iAESDecryptedSize = 0;

static nrf_crypto_aes_context_t cbc_encr_256_ctx; // AES CBC encryption context
static nrf_crypto_aes_context_t cbc_decr_256_ctx; // AES CBC decryption context



__IO TransparentState state = TS_IDLE;


ret_code_t transparent_init(void) {
ret_code_t err_code;

    NRF_LOG_INFO("%s ParkPass init", StateString(state));    
    uint32_t to = ppc_get_bleto();
    m_ble_timeout = APP_TIMER_TICKS(to);
    NRF_LOG_INFO("%s BLE Timeout %d ms", StateString(state), to);

    err_code = app_timer_create(&transparent_timer_id, APP_TIMER_MODE_SINGLE_SHOT, transparent_timer_callback);
    APP_ERROR_CHECK(err_code);
    
    err_code = app_timer_create(&timer_reset_id, APP_TIMER_MODE_SINGLE_SHOT, timer_reset_callback);
    APP_ERROR_CHECK(err_code);

    return err_code;
}


ret_code_t transparent_init_services(void) {
ble_nus_init_t     nus_init;
    // Initialize NUS.
    memset(&nus_init, 0, sizeof(nus_init));

    nus_init.data_handler = nus_data_handler;

ret_code_t err_code;
    err_code = ble_nus_init(&m_nus, &nus_init);
    APP_ERROR_CHECK(err_code);
    return err_code;
}


static void timer_reset_callback(void *p_context) {
UNUSED_PARAMETER(p_context);
    NRF_LOG_DEBUG("%s Restart to change name or DFU", StateString(state));
    NVIC_SystemReset();
}


static void transparent_timer_callback(void *p_context) {
UNUSED_PARAMETER(p_context);

    switch (state) {
        case TS_IDLE:        
            break;
        
        case TS_WAIT_OK:
            NRF_LOG_INFO("%s Crypto OK timeout", StateString(state));
            ble_disconnect(1);
            break;

        default:
            NRF_LOG_DEBUG("%s default timeout", StateString(state));
    }

}

static ret_code_t DisconnectAndReset() {
}


#define BASE64_SIZE             136
static uint8_t base64_encoded[BASE64_SIZE];


ret_code_t ble_disconnect(uint8_t status) {
    parkpass_crypto_keys_free();

    NRF_LOG_DEBUG("%s Transparent disconnect", StateString(state));
    app_timer_stop(transparent_timer_id);
    if (m_conn_handle != BLE_CONN_HANDLE_INVALID) {
        sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
    }

    if (m_expectant_conn_handle != BLE_CONN_HANDLE_INVALID) {
        sd_ble_gap_disconnect(m_expectant_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
    }
    
    state = TS_DISCONNECT_PENDING;
    NRF_LOG_DEBUG("Disconnect pending");  
    
    snprintf(m_uart_buffer_tx, UART_MAX_DATA_LEN, "cDISCONN%03d\n", status);
    transparent_uart_tx(m_uart_buffer_tx, strnlen(m_uart_buffer_tx, UART_MAX_DATA_LEN));
    return NRF_SUCCESS;
}


ret_code_t transparent_disconnect_event(uint16_t conn_handle, uint32_t reason) {

    if (conn_handle == m_expectant_conn_handle) {
        m_expectant_conn_handle = BLE_CONN_HANDLE_INVALID;
    }

    if (conn_handle == m_conn_handle) {
        m_conn_handle = BLE_CONN_HANDLE_INVALID;
    }
    
    NRF_LOG_DEBUG("%s transparent_disconnect", StateString(state));

    m_conn_handle = BLE_CONN_HANDLE_INVALID;
    m_expectant_conn_handle = BLE_CONN_HANDLE_INVALID;

    m_uart_buffer_rx_size = 0;
    m_uart_buffer_tx_size = 0;
    m_iBleRemainingBytes = 0;
    m_iUartRemainingBytes = 0;
    
    if (reason == BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION) {
        reason = 1;
    } else if (reason == BLE_HCI_LOCAL_HOST_TERMINATED_CONNECTION) {
        reason = 2;
    } else if (reason == BLE_HCI_CONN_FAILED_TO_BE_ESTABLISHED) {
        reason = 3;
    } else if (reason == BLE_HCI_CONN_TERMINATED_DUE_TO_MIC_FAILURE) {
        reason = 4;
    } else {
        reason = 0;
    }

    snprintf(m_uart_buffer_tx, UART_MAX_DATA_LEN, "cDISCONN%03d\n", 255 - reason);
    transparent_uart_tx(m_uart_buffer_tx, strnlen(m_uart_buffer_tx, UART_MAX_DATA_LEN));

    state = TS_IDLE;
    return NRF_SUCCESS;
}


#define BASE64_DECODED_MAX_SIZE  140
static uint8_t decoded_base64[BASE64_DECODED_MAX_SIZE];


ret_code_t uart_process_command(char *pdata, size_t pdata_len) {
    
    bool valid_command = false;

    char *ss = strnstr(pdata, "PING", pdata_len);
    if (ss == pdata) {
        NRF_LOG_DEBUG("%s PING received", StateString(state));
        sprintf(m_uart_buffer_tx, "cPONG\n");
        transparent_uart_tx(m_uart_buffer_tx, strlen(m_uart_buffer_tx));
        valid_command = true;
        goto check_exit;
    }

    ss = strnstr(pdata, "DISCONNECT", pdata_len);
    if (ss == pdata) {
        NRF_LOG_DEBUG("%s DISCONNECT received", StateString(state));
        sprintf(m_uart_buffer_tx, "cOK\n");
        transparent_uart_tx(m_uart_buffer_tx, strlen(m_uart_buffer_tx));
        ble_disconnect(3);
        valid_command = true;
        goto check_exit;
    }
    
    uint32_t ble_timeout_ms = 0;
    if (check_valid_BLETO(pdata, pdata_len, &ble_timeout_ms)) {
        NRF_LOG_DEBUG("%s Set BLE timeout to %d ms", StateString(state), ble_timeout_ms);
        m_ble_timeout = APP_TIMER_TICKS(ble_timeout_ms);
        ppc_set_bleto(ble_timeout_ms);
        sprintf(m_uart_buffer_tx, "cOK\n");
        transparent_uart_tx(m_uart_buffer_tx, strlen(m_uart_buffer_tx));

        valid_command = true;
        goto check_exit;
    }
    
    ss = strnstr(pdata, "VERSION", pdata_len);
    if (ss == pdata) {
        NRF_LOG_DEBUG("%s VERSION received", StateString(state));
        sprintf(m_uart_buffer_tx, "cVER:1\n");
        transparent_uart_tx(m_uart_buffer_tx, strlen(m_uart_buffer_tx));
        valid_command = true;
        goto check_exit;
    }
    
    ss = strnstr(pdata, "NAME:", pdata_len);
    if (ss == pdata) {
        NRF_LOG_DEBUG("%s NAME command", StateString(state));
        sprintf(m_uart_buffer_tx, "cOK\n");
        transparent_uart_tx(m_uart_buffer_tx, strlen(m_uart_buffer_tx));
        char *start_name = pdata + 5;
        start_name[strlen(start_name) - 1] = '\0'; // �������� ����������� '\n'
        start_name[DEVICE_SIMPLE_NAME_LEN] = '\0';
        NRF_LOG_DEBUG("%s NAME: %s", StateString(state), start_name);
        uint32_t err_code = ppc_set_name(start_name);
        if (err_code != NRF_SUCCESS) {
            NRF_LOG_DEBUG("Cannot set name");
        } else {
            app_timer_start(timer_reset_id, APP_TIMER_TICKS(5000), NULL); // 5 ������ �� ������
            NRF_LOG_DEBUG("Set timer to change name");
            DisconnectAll();
            ble_conn_params_stop();            
        }

        valid_command = true;
        goto check_exit;
    }
    
    ss = strnstr(pdata, "CONFIG", pdata_len);
    if (ss == pdata) {
        NRF_LOG_DEBUG("%s CONFIG received", StateString(state));
        
        sprintf(m_uart_buffer_tx, "cTIMEOUT:%d\n", ppc_get_bleto());
        transparent_uart_tx(m_uart_buffer_tx, strlen(m_uart_buffer_tx));

        sprintf(m_uart_buffer_tx, "cNAME:RPS_%s\n", ppc_get_name());
        transparent_uart_tx(m_uart_buffer_tx, strlen(m_uart_buffer_tx));
        
        char str[3*6 + 1];     
        ble_gap_addr_t addr;
        uint32_t err_code = sd_ble_gap_addr_get(&addr);
        if (err_code == NRF_SUCCESS) {
            sprintf(m_uart_buffer_tx, "cMAC:%02X%02X%02X%02X%02X%02X\n", 
                    addr.addr[5], addr.addr[4], addr.addr[3],
                    addr.addr[2], addr.addr[1], addr.addr[0]);
        } else {
            sprintf(m_uart_buffer_rx, "cMAC:000000000000\n");
        }
        transparent_uart_tx(m_uart_buffer_tx, strlen(m_uart_buffer_tx));

        valid_command = true;
        goto check_exit;
    }

    if (check_valid_DFU(pdata, pdata_len)) {
        NRF_LOG_DEBUG("%s Restart to DFU", StateString(state));
        sd_power_gpregret_set(0, 0xB1);
        NVIC_SystemReset();
    }

check_exit:
    if (valid_command == false) {
        NRF_LOG_DEBUG("Invalid command: %s", pdata);
        sprintf(m_uart_buffer_tx, "cINVALID\n");
        transparent_uart_tx(m_uart_buffer_tx, strlen(m_uart_buffer_tx));
    }
    return NRF_SUCCESS;
}


ret_code_t uart_process_data(uint8_t *pdata, size_t pdata_len) {   
    if (state != TS_DATA_EXCANGE) {
        NRF_LOG_ERROR("%s Ignoring data package", StateString(state))
        sprintf(m_uart_buffer_tx, "cNOTCONNECTED\n");
        transparent_uart_tx(m_uart_buffer_tx, strlen(m_uart_buffer_tx));
        return NRF_SUCCESS;
    }

    size_t olen = 0;
    int base_ret = mbedtls_base64_decode(decoded_base64, sizeof(decoded_base64), &olen, pdata + 1, pdata_len - 1);
    if (base_ret != 0) {
        NRF_LOG_ERROR("Base64 decode error: %d", base_ret);
        ble_disconnect(4);
    } else {
        NRF_LOG_DEBUG("Decoded %d bytes", olen);
        transparent_ble_transmit(m_conn_handle, decoded_base64, olen, 0, false, true);
    }
    return NRF_SUCCESS;
}


ret_code_t uart_process_byte(void) {
uint8_t symbol;
    if (m_uart_buffer_rx_size < UART_MAX_DATA_LEN - 1) {
        app_uart_get(&symbol);
        m_uart_buffer_rx[m_uart_buffer_rx_size] = symbol;
        m_uart_buffer_rx[m_uart_buffer_rx_size + 1] = '\0';
        m_uart_buffer_rx_size++;
        
        if (symbol == '\n') {
            if (m_uart_buffer_rx[0] == 'c') {
                NRF_LOG_DEBUG("%s UART Receive command", StateString(state));
                uart_process_command(m_uart_buffer_rx + 1, m_uart_buffer_rx_size - 1);
            }

            if (m_uart_buffer_rx[0] == 'd') {
                NRF_LOG_DEBUG("%s UART Receive data", StateString(state));
                uart_process_data(m_uart_buffer_rx, m_uart_buffer_rx_size);
            }
            m_uart_buffer_rx_size = 0;
        }
    } else {
        m_uart_buffer_rx_size = 0;
    }

    return NRF_SUCCESS;
}


void uart_event_handle(app_uart_evt_t * p_event)
{
    switch (p_event->evt_type)
    {
        case APP_UART_DATA_READY: {
            uart_process_byte();
        } break;

        case APP_UART_COMMUNICATION_ERROR:
            NRF_LOG_DEBUG("UART ERROR");
            app_uart_flush();
            //APP_ERROR_HANDLER(p_event->data.error_communication);
            break;

        case APP_UART_FIFO_ERROR:
            APP_ERROR_HANDLER(p_event->data.error_code);
            break;

        default:
            break;
    }
}

static void transparent_uart_tx(const uint8_t *data, size_t len) {
    for (size_t i = 0; i < len; i++) {
        while (app_uart_put(data[i]) != NRF_SUCCESS);
    }
}


ret_code_t ble_process_in_message(uint8_t *data, size_t data_size, uint16_t conn_handle) {
UNUSED_PARAMETER(data);
UNUSED_PARAMETER(data_size);

char *pdata = data;
uint16_t len = data_size;
ret_code_t err_code;

    app_timer_stop(transparent_timer_id);
    
    if (state == TS_WAIT_INKAS) {
        // conn_handle - ����������
        pdata[len] = 0;
        size_t key_size = 0;
        if (check_valid_INKAS_BLE(pdata, len, parkpass_crypto_remote_public_raw(), &key_size, ECDH_PUBLIC_KEY_SIZE)) {
            err_code = parkpass_crypto_keys_generate();
            if (err_code != NRF_SUCCESS) {
                NRF_LOG_DEBUG("Key pair not generated");
                ble_disconnect(5);
                return NRF_SUCCESS;
            }

            if (parkpass_crypto_keys_calculate_shared()) {
                NRF_LOG_DEBUG("Shared key generation error");
                ble_disconnect(6);
                return NRF_SUCCESS;
            }

            parkpass_crypto_print_keys();

            NRF_LOG_DEBUG("Expectant handle Found");
            m_expectant_conn_handle = conn_handle;

            snprintf(m_uart_buffer_tx, UART_MAX_DATA_LEN, "cEXPECTANT\n");
            transparent_uart_tx(m_uart_buffer_tx, strnlen(m_uart_buffer_tx, UART_MAX_DATA_LEN));
            
            size_t olen = 0;
            err_code = mbedtls_base64_encode(base64_encoded, sizeof(base64_encoded), &olen, parkpass_crypto_local_public_raw(), ECDH_PUBLIC_KEY_SIZE);
            if (err_code != NRF_SUCCESS) {
                ble_disconnect(7);
                return NRF_SUCCESS;
            }

            memset(m_uart_buffer_tx, 0, UART_MAX_DATA_LEN);
            snprintf(m_uart_buffer_tx, UART_MAX_DATA_LEN, "PPINKASCM:%s", base64_encoded);
            app_timer_start(transparent_timer_id, m_ble_timeout, NULL);

            transparent_ble_transmit(m_expectant_conn_handle, m_uart_buffer_tx, strnlen(m_uart_buffer_tx, UART_MAX_DATA_LEN), 0, false, false);

            state = TS_WAIT_OK;
            return NRF_SUCCESS;
        } else {
            NRF_LOG_DEBUG("%s Invalid INKAS", StateString(state));
        }
    }

    if (state == TS_WAIT_OK) {
        pdata[len] = 0;

        char *ss = strnstr(pdata, "OK", len);
        size_t ok_len = strnlen(pdata, len);
        NRF_LOG_DEBUG("OK len: %d", len);
        if (ss != pdata) {
            NRF_LOG_DEBUG("%s Crypto OK invalid", StateString(state));
            ble_disconnect(8);
            return NRF_SUCCESS;
        }

        NRF_LOG_DEBUG("%s Transparent found", StateString(state));
        m_conn_handle = conn_handle;
        
        snprintf(m_uart_buffer_tx, UART_MAX_DATA_LEN, "cINKASCMOK\n");
        transparent_uart_tx(m_uart_buffer_tx, strnlen(m_uart_buffer_tx, UART_MAX_DATA_LEN));
        
        state = TS_DATA_EXCANGE;
        return NRF_SUCCESS;
    }
    
    if (state == TS_DATA_EXCANGE) {
        pdata[len] = 0;
        NRF_LOG_DEBUG("%s Data exchange!! Encode and send throw UART", StateString(state));
        size_t olen = 0;
        int base_ret = mbedtls_base64_encode(m_uart_buffer_tx + 1, sizeof(m_uart_buffer_tx) - 1, &olen, pdata, len);
        if (base_ret != 0) {
            NRF_LOG_ERROR("Cannot encode BLE message: %d", base_ret);
        } else {
            NRF_LOG_DEBUG("BLE message encoded and transmit throw UART. Size %d bytes", olen);
            m_uart_buffer_tx[0] = 'd';
            m_uart_buffer_tx[1 + olen + 0] = '\n';
            m_uart_buffer_tx[1 + olen + 1] = '\0';
            transparent_uart_tx(m_uart_buffer_tx, olen + 2);
        }
    }
    return NRF_SUCCESS;
}



void nus_data_handler(ble_nus_evt_t * p_evt)
{   

    if (p_evt->type == BLE_NUS_EVT_RX_DATA)
    {
        uint32_t err_code;

        NRF_LOG_DEBUG("%s BLE Rx (0x%X), %d bytes", StateString(state), p_evt->conn_handle, p_evt->params.rx_data.length);
        NRF_LOG_DEBUG("%s BLE Rx AES:", StateString(state));
        const uint8_t *p_data = p_evt->params.rx_data.p_data;
        uint16_t data_len = p_evt->params.rx_data.length;
        NRF_LOG_HEXDUMP_DEBUG(p_data, data_len >= 16 ? 16 : data_len);
        
        if (((state == TS_IDLE) || (state == TS_WAIT_INKAS)) && (m_expectant_conn_handle == BLE_CONN_HANDLE_INVALID)) {
            // ����� �� ����������.
            m_iAESDecryptedSize = data_len;
            memcpy(m_pbAESDecryptedBuffer, p_data, data_len);
        } else {
            // ����� ����������
            size_t out_size = AES_BUFFERS_SIZE;
            err_code = parkpass_decrypt(&cbc_decr_256_ctx, p_data, data_len, m_pbAESDecryptedBuffer, &out_size);
            NRF_LOG_DEBUG("%s pp_decrypt return: 0x%04X: %s", StateString(state), err_code, nrf_crypto_error_string_get(err_code));
            
            if ((err_code != NRF_SUCCESS) || (out_size > AES_BUFFERS_SIZE)) {
                ble_disconnect(9);
                return;
            }
            m_iAESDecryptedSize = out_size; 
        }
        ble_process_in_message(m_pbAESDecryptedBuffer, m_iAESDecryptedSize, p_evt->conn_handle);
    }
}


static ret_code_t transparent_ble_transmit(uint16_t conn_handle, uint8_t *data, size_t len, uint32_t timeout, bool startTimer, bool crypt) {
UNUSED_PARAMETER(timeout);
UNUSED_PARAMETER(startTimer);

ret_code_t err_code;
    
    if (crypt == true) {
#if CRYPTO == 1
        m_iAESEncryptedSize = AES_BUFFERS_SIZE;
        NRF_LOG_DEBUG("%s ble_tx before AES: %d bytes", StateString(state), len);
        parkpass_encrypt(&cbc_encr_256_ctx, data, len, m_pbAESEncryptedBuffer, &m_iAESEncryptedSize);
        NRF_LOG_DEBUG("%s ble_tx AES: %d bytes", StateString(state), m_iAESEncryptedSize);
#endif
    } else {
        memcpy(m_pbAESEncryptedBuffer, data, len);
        m_iAESEncryptedSize = len;
    }

    do {
#if CRYPTO == 1
        err_code = ble_nus_data_send(&m_nus, m_pbAESEncryptedBuffer, &m_iAESEncryptedSize, conn_handle);
#else
        err_code = ble_nus_data_send(&m_nus, data, &len, conn_handle);
#endif
        if ((err_code != NRF_ERROR_INVALID_STATE) && (err_code != NRF_ERROR_BUSY) && (err_code != NRF_ERROR_NOT_FOUND))
             APP_ERROR_CHECK(err_code);
    } while (err_code == NRF_ERROR_BUSY);

    return NRF_SUCCESS;
}


const char *StateString(TransparentState state) {
    switch (state) {
        case TS_IDLE:
            return "<TS_IDLE, TS_WAIT_INKAS>";
            break;

        case TS_WAIT_OK:
            return "<TS_WAIT_OK>";
            break;

        case TS_DATA_EXCANGE:
            return "<TS_DATA_EXCANGE>";
            break;

        case TS_DISCONNECT_PENDING:
            return "<TS_DISCONNECT_PENDING>";
            break;
        
        default:
            return "<UNKNOWN State>";
    }

    return "<UNKNOWN>";
}
