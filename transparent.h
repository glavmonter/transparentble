#ifndef PARKPASS2_H_
#define PARKPASS2_H_

#include <nrf_error.h>
#include <sdk_errors.h>
#include "ble_nus.h"

#include "app_uart.h"
#include "app_util_platform.h"
#if defined (UART_PRESENT)
#include "nrf_uart.h"
#endif
#if defined (UARTE_PRESENT)
#include "nrf_uarte.h"
#endif


typedef enum _TransparentState {
    TS_IDLE = 0,
    TS_WAIT_INKAS = TS_IDLE,
    TS_WAIT_OK,
    TS_DATA_EXCANGE,

    TS_DISCONNECT_PENDING
} TransparentState;


const char *StateString(TransparentState state);


void nus_data_handler(ble_nus_evt_t * p_evt);
ret_code_t transparent_init(void);
ret_code_t transparent_init_services(void);

ret_code_t transparent_disconnect_event(uint16_t conn_handle, uint32_t reason);

void uart_event_handle(app_uart_evt_t * p_event);
bool check_valid_SVC(const char *data, size_t max_len, int8_t *rssi_threshold, int8_t *rssi_delta);

#endif // PARKPASS2_H_
